# English

> [!NOTE]  
> I change things of each backend very often and some backends can fail so this list is not always accurate.

My instance allows you to choose the backend you want to use. The details and differences between them go as follows: 

## Backends

| Backend | Companion Location |
|----------|--------------------------------|
| Backend 1 [inv1.nadeko.net](https://inv1.nadeko.net)  | Chile                                                                
| Backend 2 [inv2.nadeko.net](https://inv2.nadeko.net)  | Toronto, CA                                                          
| Backend 3 [inv3.nadeko.net](https://inv3.nadeko.net)  | Texas, USA                                                      
| Backend 4 [inv4.nadeko.net](https://inv4.nadeko.net)  | Amsterdam, NL                                                  
| Backend 5 [inv5.nadeko.net](https://inv5.nadeko.net) | Frankfurt, DE          


**Companion Location** means from where the video information is being gathered, not where the backend is hosted physically. All backends are hosted on the same server and on the same location, which is Chile of course. 

How fast the video loads depends of the Companion Location, so if you are from Europe, you may prefer to use Backend 4 or Backend 5. If you want to access geo restricted content that is only available on the US, then use Backend 3.

# Español

> [!NOTE]  
> Cambio cosas de los backends muy a menudo y algunos backends pueden fallar por lo que esta lista no siempre es exacta.

Mi instancia te permite elegir el backend que quieras utilizar. Los detalles y diferencias entre ellos son los siguientes: 

## Backends

| Backend | Localización de Companion |
|----------|--------------------------------|
| Backend 1 [inv1.nadeko.net](https://inv1.nadeko.net)  | Chile                                                                
| Backend 2 [inv2.nadeko.net](https://inv2.nadeko.net)  | Toronto, CA                                                          
| Backend 3 [inv3.nadeko.net](https://inv3.nadeko.net)  | Texas, EEUU                                                    
| Backend 4 [inv4.nadeko.net](https://inv4.nadeko.net)  | Amsterdam, NL                                                  
| Backend 5 [inv5.nadeko.net](https://inv5.nadeko.net) | Frankfurt, DE            


**Localización de Companion** significa desde dónde se obtiene la información de vídeo, no dónde está alojado físicamente el backend. Todos los backends están alojados en el mismo servidor y en la misma ubicación, que es Chile, obviamente.

La velocidad de carga del video depende de la Localización de Companion, por lo que si eres de Europa, es posible que prefieras utilizar el Backend 4 o el Backend 5. Si quieres acceder a contenido geo restringido que sólo está disponible en EEUU, entonces usa el Backend 3.

---
