Hi @Fijxu here. Youtube changed the method they use to block instances and third party clients so I have been struggling a bit on how to make the instance work fluently as it did before for a long time.

So because of that, Invidious is extremely limited and prone to fail. Thanks to the backend system that I made, I'm able to balance the load across different servers outside of my own homeserver to prevent blockages (if I only had one Backend, my instance would be extremely painful to use). 

Also, the subscriptions tab will not contain the video length and view count, this is because Invidious used to do a request to Youtube to get more information about the video (like the view count and video length) to display it on the subscriptions tab. I had to change this to prevent getting blocked and continue to receive subscription updates. (Commit https://git.nadeko.net/Fijxu/invidious/commit/df94f1c0b82d95846574487231ea251530838ef0 for the nerds)

![](https://culiao.lol/mFM)

I also would like to ask Bot/API developers to not use my instance to get information about a video from Youtube as this creates more requests to Youtube therefore blocking the Backend of the instance. Please use tools like https://github.com/LuanRT/YouTube.js or any other tool that lets you get video information from Youtube. ~~I will start blocking/rate-limiting generic User-Agents that are generally used on http libraries like `axios/1.x.x`, `python-requests`, `Go-http-client/2.0` and so on. Please be ethical and don't spoof your User-agent to make it look like a browser!; Just change it to something that identifies your program.~~ API has been disabled for everyone due to the high influx of API requests and instability of the instance.

Also, if you want to download videos a ton of videos, please don't use Invdious! Use https://github.com/yt-dlp/yt-dlp or https://cobalt.tools/ (Donate if you find this useful, BANDWIDTH COSTS ARE NOT CHEAP!)

If you use Invidious a lot and you have the knowledge and resources to host Invidious yourself, I recommend you to do it instead of struggling with the constant blockages of my instance.

I hope you all can understand <3

---
