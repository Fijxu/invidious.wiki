Sooo. Numbered backends are here, you will now be able to use the backend you to want use using a domain instead of a cookie. This approach is way better if you use FreeTube or any other third party invidious client (and also useful for people that use the API, but if you are using my Instance to gather information from channels or videos I really recommend you to use https://github.com/LuanRT/YouTube.js instead of invidious).

The domain list is as follows:
- https://inv1.nadeko.net
- https://inv2.nadeko.net
- https://inv3.nadeko.net
- https://inv4.nadeko.net
- https://inv5.nadeko.net

You will need to relogin into Invidious in each backend since they use cookies that are fixed to the domain.
Also, you can ignore the value of `You are currently using Backend` because that value comes from the `INVIDIOUS_SERVER_ID` cookie. I need to change that lol

